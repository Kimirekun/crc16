﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace CRC16
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string strg = richTextBox1.Text;
            if (strg[strg.Length - 1] == ' ')
            {
                strg = strg.Substring(0, strg.Length - 1);
            }
            if (strg[strg.Length - 1] == ' ')
            {
                MessageBox.Show("?");
            }
            string[] datas = strg.Split(' ');
            List<byte> bytedata = new List<byte>();
            foreach (string str in datas)
            {
                bytedata.Add(byte.Parse(str, System.Globalization.NumberStyles.AllowHexSpecifier));
            }
            byte[] crcbuff = bytedata.ToArray();
            int crc = 0xffff;
            int len = crcbuff.Length;
            for (int n = 0;n < len;n ++)
            {
                byte i;
                crc = crc ^ crcbuff[n];
                for (i = 0;i < 8;i++)
                {
                    int TT;
                    TT = crc & 1;
                    crc = crc >> 1;
                    crc = crc & 0x7fff;
                    if (TT == 1)
                    {
                        crc = crc ^ 0xa001;
                    }
                    crc = crc & 0xffff;
                }
            }
            string[] redata = new string[2];
            redata[1] = Convert.ToString(((byte)((crc >> 8)) & 0xff),16);
            redata[0] = Convert.ToString(((byte)(crc & 0xff)), 16);
            textBox1.Text = redata[1].ToUpper() +  " " + redata[0].ToUpper();
        }
    }
}
